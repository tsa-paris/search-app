# go-elastic-api

## Description
This is a go-kit powered microservice API that inserts/update data into either a sqlite or a elasticsearch database

## Pre-requisities
For dev purposes:
- having golang installed on your machine.

If you want to run the microservice using elasticsearch, you need to have a elasticsearch server running.

The command example below assumes that an elasticsearch server is running under `http://localhost:92000`

## Setup
Run the command 
```console
$ go get ./...
```

## How to use: 
### Commands to run the microservice
- run `go run main.go` will connect to a sqlite3 database
or if you want to use elasticsearch
- set environment variables by running the following command in bash: 
```console
$ TYPE_DB=elasticsearch URL_DB=http://localhost:9200 PORT=8080 go run main.go
```
or in fish : 
```console
$ env TYPE_DB=elasticsearch env URL_DB=http://localhost:9200 env PORT=8080 go run main.go
```

## Building the package
``` 
$ go build main.go
```

## Testing
To execute the unit tests and to fetch dependencies run...
```
$ go run test
```

## Try to insert some data
With for example postman,
Make a POST request to the url `http://localhost:8080/tour`
```json
{
	"id": 1000,
	"code": "Code Test",
	"name": "Name to test",
	"agent": "Agent name test",
	"start": "2016-10-01",
	"content": "Here comes a more complete text content",
	"url": "http://www.test.com"
}
```

## Spin up with docker-compose
- Development mode
	
	To build 
	```bash
	docker-compose build
	```
	To run 
	```bash
	docker-compose run -d
	```
- Production mode
	
	(TO-DO)