package main

import (
	"context"
	"flag"
	"fmt"

	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/pkg/errors"

	esarch "gitlab.com/tsa-paris/go-elastic-api/repository/elasticsearch"
	sqlite "gitlab.com/tsa-paris/go-elastic-api/repository/sqlite"

	"gitlab.com/tsa-paris/go-elastic-api/api"
	"gitlab.com/tsa-paris/go-elastic-api/tours"
)

const (
	defaultPort             = "8080"
	defaultDB               = "sqlite"
	defaultElasticsearchURL = "http://localhost:9200"
)

func main() {
	var (
		port     = envString("APP_PORT", defaultPort)
		httpAddr = flag.String("http.addr", ":"+port, "HTTP listen address")
		ctx      = context.Background()
	)

	flag.Parse()
	repoType := envString("TYPE_DB", defaultDB)
	var repo, repoErr = chooseRepo(repoType)
	if repoErr != nil {
		fmt.Printf("Exiting the application: %s\n", repoErr)
		os.Exit(1)
	}
	var service = tours.NewService(repo)

	errChan := make(chan error)

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()

	endpoints := tours.MakeEndpoints(service)

	go func() {
		fmt.Println("listening on port", *httpAddr)
		handlers := api.NewRouter(ctx, endpoints)
		errChan <- http.ListenAndServe(*httpAddr, handlers)
	}()

	fmt.Printf("Terminated %s", <-errChan)
}

func envString(env, fallback string) string {
	e := os.Getenv(env)
	if e == "" {
		return fallback
	}
	return e
}

func chooseRepo(repoType string) (tours.Repository, error) {
	fmt.Printf("Chosen repository type: %s\n", repoType)
	switch repoType {
	case "", "sqlite":
		fmt.Printf("\"TYPE_DB\" not specified\nUsing sqlite database as default\n")
		filePath := "./database/sqlite.db"
		repo, err := sqlite.NewSqliteRepository(filePath)
		if err != nil {
			// stdlog.Fatal(err)
			return nil, err
		}
		return repo, nil
	case "elasticsearch":
		urlDB := envString("URL_DB", defaultElasticsearchURL)
		repo, err := esarch.NewElasticsearchRepository(urlDB)
		if err != nil {
			fmt.Printf("\"TYPE_DB\" not specified\nUsing sqlite database as default\n")
			// stdlog.Fatal(err)
			return nil, err
		}
		return repo, nil
	}
	return nil, errors.New("Repository type not found")
}
