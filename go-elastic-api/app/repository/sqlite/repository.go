package sqlite

import (
	"context"
	"database/sql"

	"github.com/pkg/errors"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/tsa-paris/go-elastic-api/tours"
)

var errRepo = errors.New("Unable to handle Repo Request")

type sqliteRepository struct {
	client *sql.DB
}

func newSqliteClient(filePath string) (*sql.DB, error) {
	var db *sql.DB
	var err error
	db, err = sql.Open("sqlite3", filePath)
	if err != nil {
		return nil, err
	}

	return db, nil
}

// NewSqliteRepository is the function that will create the repository
func NewSqliteRepository(filePath string) (tours.Repository, error) {
	repo := &sqliteRepository{}
	client, err := newSqliteClient(filePath)
	if err != nil {
		return nil, errors.Wrap(err, "repository.NewSqliteRepository")
	}
	repo.client = client
	return repo, nil
}

func (respo *sqliteRepository) Status(ctx context.Context) (string, error) {
	return "ok", nil
}

func (repo *sqliteRepository) InsertTour(ctx context.Context, tour tours.Tour) error {
	// agent TEXT NOT NULL UNIQUE
	createTableQuery := `
	CREATE TABLE IF NOT EXISTS tours (
		id TEXT PRIMARY KEY,
		code TEXT NOT NULL,
		name TEXT NOT NULL,
		agent TEXT NOT NULL,
		start TEXT NOT NULL,
		content TEXT NOT NULL,
		url TEXT NOT NULL
	);`

	if _, err := repo.client.ExecContext(ctx, createTableQuery); err != nil {
		return err
	}

	sql := `
		INSERT INTO tours (id, code, name, agent, start, content, url)
		VALUES ($1, $2, $3, $4, $5, $6, $7)`

	if tour.Code == "" || tour.Name == "" || tour.Agent == "" || tour.Start == "" || tour.Content == "" || tour.URL == "" {
		return errors.Wrap(errRepo, "One or more fields are empty")
	}

	_, err := repo.client.ExecContext(ctx, sql, tour.ID, tour.Code, tour.Name, tour.Agent, tour.Start, tour.Content, tour.URL)
	if err != nil {
		return err
	}
	return nil
}

func (repo *sqliteRepository) DeleteTour(ctx context.Context, id string) error {
	sql := `
		DELETE FROM tours 
		WHERE id = $1`

	if id == "" {
		return errors.Wrap(errRepo, "The ID field cannot be an empty string")
	}

	res, err := repo.client.ExecContext(ctx, sql, id)
	if err != nil {
		return err
	}
	count, execErr := res.RowsAffected()
	if execErr != nil {
		return errors.Wrap(execErr, "Error executing Delete sql command")
	}
	if count == 0 {
		return errors.New("No records have been deleted")
	}
	return nil
}
