package elasticsearch

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/pkg/errors"

	"github.com/olivere/elastic/v7"

	"gitlab.com/tsa-paris/go-elastic-api/tours"
)

type elasticRepository struct {
	client    *elastic.Client
	url       string
	indexName string
}

func newElasticClient(url string) (*elastic.Client, error) {

	client, err := elastic.NewClient(
		elastic.SetURL(url),
		// elastic.SetHealthcheckInterval(10*time.Second),
		elastic.SetGzip(true),
		// elastic.SetHealthcheckTimeoutStartup(10*time.Second),
		// elastic.SetHealthcheckInterval(10*time.Second),
		elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)),
		elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)),
	)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// NewElasticsearchRepository is the function that will create the repository
func NewElasticsearchRepository(url string) (tours.Repository, error) {
	repo := &elasticRepository{}
	log.Printf("Connecting to elasticsearch server on: %s\n", url)
	client, err := newElasticClient(url)
	if err != nil {
		return nil, errors.Wrap(err, "repository.NewElasticsearchRepository")
	}
	repo.client = client
	repo.url = url
	repo.indexName = "tour"
	if initErr := repo.initialize(); err != nil {
		return nil, initErr
	}
	return repo, nil
}

func (repo *elasticRepository) initialize() error {
	log.Println("Index \"tour\" doesn't exist yet. Creating it")
	defer log.Println("Index \"tour\" successfuly created")
	return repo.createTourIndex()
}

func (repo *elasticRepository) Status(ctx context.Context) (string, error) {
	info, code, err := repo.client.Ping(repo.url).Do(ctx)
	if err != nil {
		panic(err)
	}
	msg := fmt.Sprintf("Elasticsearch returned with code %d - version %s", code, info.Version.Number)
	return msg, nil
}

func (repo *elasticRepository) InsertTour(ctx context.Context, tour tours.Tour) error {

	exists, err := repo.client.IndexExists(repo.indexName).Do(context.Background())
	if err != nil {
		return errors.Wrap(err, "repository.elasticRepository.InsertTour")
	}
	if !exists {
		// Index does not exist yet.
		return errors.Wrap(err, "repository.elasticRepository.InsertTour")
	}

	_, addErr := repo.client.Index().
		Index(repo.indexName).
		// Type("_doc").
		Id(tour.ID).
		BodyJson(tour).
		Do(ctx)
	if addErr != nil {
		// Handle error
		// panic(err)
		// return addErr
		log.Println(addErr.Error())
		return errors.Wrap(addErr, "repository.elasticRepository.InsertTour")
	}

	return nil
}

func (repo *elasticRepository) DeleteTour(ctx context.Context, id string) error {
	_, err := repo.client.Delete().
		Index(repo.indexName).
		Id(id).
		Do(ctx)
	if err != nil {
		// Handle error
		log.Println(err.Error())
		return errors.Wrap(err, "repository.elasticRepository.DeleteTour")
	}

	return nil
}

func (repo *elasticRepository) createTourIndex() error {
	// Create a new index.
	mapping := `{
		"settings":{
			"index": {
				"number_of_shards":1,
				"number_of_replicas":1
			}
		},
		"mappings":{
			"properties":{
				"id": {
					"type": "text"
				},
				"code": {
					"type": "text"
				},
				"name": {
					"type": "text"
				},
				"agent": {
					"type": "text"
				},
				"start": {
					"type": "date"
				},
				"content": {
					"type": "text"
				},
				"url": {
					"type": "text"
				}
			}
		}
	}`

	ctx := context.Background()
	createIndex, err := repo.client.CreateIndex(repo.indexName).BodyString(mapping).Do(ctx)
	if err != nil {
		// Handle error
		return errors.Wrap(err, "repository.elasticRepository.createTourIndex")
	}
	if !createIndex.Acknowledged {
		// Not acknowledged
		return errors.New("Could not aknowledge the creation of the index. repository.elasticRepository.createTourIndex")
	}

	return nil
}
