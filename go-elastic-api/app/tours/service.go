package tours

import "context"

type Service interface {
	Status(ctx context.Context) (string, error)
	InsertTour(ctx context.Context, req InsertTourRequest) (string, error)
	DeleteTour(ctx context.Context, req DeleteTourRequest) (string, error)
}
