package tours

import (
	"context"
	"encoding/json"
	"net/http"
)

type (
	// InsertTourRequest structure
	InsertTourRequest struct {
		ID      int    `json:"id"`
		Code    string `json:"code"`
		Name    string `json:"name"`
		Agent   string `json:"agent"`
		Start   string `json:"start"`
		Content string `json:"content"`
		URL     string `json:"url"`
	}

	// DeleteTourRequest structure
	DeleteTourRequest struct {
		ID int `json:"id"`
	}

	// InsertTourResponse structure
	InsertTourResponse struct {
		Ok string `json:"ok"`
	}

	// DeleteTourResponse structure
	DeleteTourResponse struct {
		Ok string `json:"ok"`
	}

	StatusRequest struct{}

	StatusResponse struct {
		Status string `json:"status"`
	}
)

func EncodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func DecodeInsertTourReq(ctx context.Context, r *http.Request) (interface{}, error) {
	var req InsertTourRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func DecodeDeleteTourReq(ctx context.Context, r *http.Request) (interface{}, error) {
	var req DeleteTourRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func DecodeStatusRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req StatusRequest
	return req, nil
}
