package tours

import (
	"context"
	"strconv"

	// "github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

type service struct {
	repository Repository
}

// NewService is a function that creates and return the Tour service
func NewService(rep Repository) Service {
	return &service{
		repository: rep,
	}
}

func (s service) Status(ctx context.Context) (string, error) {
	return s.repository.Status(ctx)
}

func (s service) InsertTour(ctx context.Context, req InsertTourRequest) (string, error) {
	id := req.ID
	// uuid, _ := uuid.NewV4()
	// id := uuid.String()
	tour := Tour{
		ID:      strconv.FormatInt(int64(id), 10),
		Code:    req.Code,
		Name:    req.Name,
		Agent:   req.Agent,
		Start:   req.Start,
		Content: req.Content,
		URL:     req.URL,
	}

	err := s.repository.InsertTour(ctx, tour)
	if err != nil {
		return "", errors.Wrap(err, "service.Tour.InsertTour")
	}

	return "Success", nil
}

func (s service) DeleteTour(ctx context.Context, req DeleteTourRequest) (string, error) {
	id := strconv.FormatInt(int64(req.ID), 10)
	err := s.repository.DeleteTour(ctx, id)
	if err != nil {
		return "", errors.Wrap(err, "service.Tour.DeleteTour")
	}

	return "Success", nil
}
