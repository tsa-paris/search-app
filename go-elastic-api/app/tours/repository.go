package tours

import "context"

// Repository interface for the Tour
type Repository interface {
	Status(ctx context.Context) (string, error)
	InsertTour(ctx context.Context, tour Tour) error
	DeleteTour(ctx context.Context, id string) error
}
