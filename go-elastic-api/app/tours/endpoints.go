package tours

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

type Endpoints struct {
	InsertTourEndPoint endpoint.Endpoint
	DeleteTourEndPoint endpoint.Endpoint
	StatusEndpoint     endpoint.Endpoint
}

func MakeEndpoints(s Service) Endpoints {
	return Endpoints{
		InsertTourEndPoint: makeInsertTourEndpoint(s),
		DeleteTourEndPoint: makeDeleteTourEndpoint(s),
		StatusEndpoint:     makeStatusEndpoint(s),
	}
}

func makeInsertTourEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(InsertTourRequest)
		ok, err := s.InsertTour(ctx, req)
		return InsertTourResponse{Ok: ok}, err
	}
}

func makeDeleteTourEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteTourRequest)
		ok, err := s.DeleteTour(ctx, req)
		return DeleteTourResponse{Ok: ok}, err
	}
}

// MakeStatusEndpoint returns the response from our service "status"
func makeStatusEndpoint(srv Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(StatusRequest)
		s, err := srv.Status(ctx)
		if err != nil {
			return StatusResponse{s}, err
		}
		return StatusResponse{s}, nil
	}
}
