package tours

// Tour is the Structure that will be inserted into Elasticsearch
type Tour struct {
	ID      string `json:"id,omitempty"`
	Code    string `json:"code"`
	Name    string `json:"name"`
	Agent   string `json:"agent"`
	Start   string `json:"start"`
	Content string `json:"content"`
	URL     string `json:"url"`
}
