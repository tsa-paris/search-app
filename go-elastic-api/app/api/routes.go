package api

import (
	"context"
	"net/http"

	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	"gitlab.com/tsa-paris/go-elastic-api/tours"
)

// NewRouter is a function that returs a Gorilla router
func NewRouter(ctx context.Context, endpoints tours.Endpoints) http.Handler {
	r := mux.NewRouter()
	r.Use(commonMiddleware)

	r.Methods("POST").Path("/tour").Handler(httptransport.NewServer(
		endpoints.InsertTourEndPoint,
		tours.DecodeInsertTourReq,
		tours.EncodeResponse,
	))

	r.Methods("DELETE").Path("/tour").Handler(httptransport.NewServer(
		endpoints.DeleteTourEndPoint,
		tours.DecodeDeleteTourReq,
		tours.EncodeResponse,
	))

	r.Methods("GET").Path("/status").Handler(httptransport.NewServer(
		endpoints.StatusEndpoint,
		tours.DecodeStatusRequest,
		tours.EncodeResponse,
	))

	return r

}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
