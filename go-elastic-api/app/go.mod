module gitlab.com/tsa-paris/go-elastic-api

go 1.13

require (
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/olivere/elastic/v7 v7.0.8
	github.com/pkg/errors v0.8.1
)
