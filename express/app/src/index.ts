import { ApolloServer } from "apollo-server-express";
import Express from "express";
import "reflect-metadata";
import { createSchema } from "./utils/createSchema";
import createCors from "./utils/createCors";

const { env } = process;
const port = env.PORT || 4000;

const main = async () => {
  const schema = await createSchema();

  const apolloServer = new ApolloServer({
    schema
    // formatError
    // context: ({ req, res }: any) => ({
    //   req,
    //   res,
    // }),
  });

  const app = Express();

  app.use((req, res, next) => {
    req.headers.origin = req.headers.origin || req.headers.host;
    next();
  });

  app.use(createCors());

  apolloServer.applyMiddleware({ app });

  app.listen(port, () => {
    console.log(`server started on http://localhost:${port}/graphql`);
  });
};

main().catch(err => console.error(err));
