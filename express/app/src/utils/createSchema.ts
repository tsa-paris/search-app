import { buildSchema } from "type-graphql";
import Container from "typedi";

import { ElasticSearchResolver } from "../modules/elasticsearch/elasticsearch-resolvers";

export const createSchema = () =>
  buildSchema({
    resolvers: [ElasticSearchResolver],
    container: Container
  });
