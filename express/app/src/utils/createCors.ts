import cors from "cors";

const whitelistRegEx = [/127.0.0.1/, /localhost/, /0.0.0.0/, /tsa-paris\.fr/];
const whitelistString = ["::1"];

const createCors = () => {
  const corsOptions: cors.CorsOptions = {
    origin: function(
      origin: string | undefined,
      callback: (err: Error | null, allow?: boolean | undefined) => void
    ) {
      if (!origin || typeof origin === "undefined") {
        callback(new Error("Not allowed by CORS"));
        return;
      }

      if (
        whitelistRegEx.findIndex(r => r.test(origin)) !== -1 ||
        whitelistString.indexOf(origin) !== -1
      ) {
        callback(null, true);
      } else {
        callback(new Error("Not allowed by CORS"));
      }
    }
  };
  return cors(corsOptions);
};

export default createCors;
