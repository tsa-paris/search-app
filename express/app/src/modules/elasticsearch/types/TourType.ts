import { ObjectType, Field } from "type-graphql";

@ObjectType()
export class TourType {
  @Field()
  id: number;

  @Field()
  agent: string;

  @Field()
  code: string;

  @Field()
  name: string;

  @Field()
  start: string;

  @Field()
  content: string;

  @Field()
  url: string;

  // @Field(() => ServiceType)
  // services: ServiceType[]
}
