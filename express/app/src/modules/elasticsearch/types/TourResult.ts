import { ObjectType, Field } from "type-graphql";
import { TourType } from "./TourType";

export const tourIndexName = "tour";
export const tourSearchFields = [
  "code",
  "name",
  "services.code",
  "services.full"
];

@ObjectType({ description: "TSA Tour search result Type" })
export class TourResult {
  @Field({ nullable: true })
  _id: string;

  @Field({ nullable: true })
  _index: string;

  @Field({ nullable: true })
  _type: string;

  @Field({ nullable: true })
  _score: number;

  @Field({ nullable: true })
  _source: TourType;
}
