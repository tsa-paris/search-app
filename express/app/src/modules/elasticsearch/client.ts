import { Client } from "@elastic/elasticsearch";

const ELASTIC_URL = "http://elasticsearch";
const ELASTIC_PORT = 9200;
// const ELASTIC_USERNAME = process.env.ELASTIC_USERNAME || "username";
// const ELASTIC_PASSWORD = process.env.ELASTIC_PASSWORD || "password";

/**
 * *** ElasticSearch *** client
 * @type {Client}
 */
const client = new Client({
  node: `${ELASTIC_URL}:${ELASTIC_PORT}`
  // auth: {
  //   username: ELASTIC_USERNAME,
  //   password: ELASTIC_PASSWORD
  // }
});

export default client;
