import { Arg, Resolver, Query } from "type-graphql";
import { TourResult } from "./types/TourResult";
import TourSearchService from "./services/TourSearchService";

@Resolver()
export class ElasticSearchResolver {
  constructor(private readonly tourSearchService: TourSearchService) {}
  @Query(() => [TourResult])
  async searchAll(
    @Arg("term", { nullable: true }) term: string
  ): Promise<TourResult[] | null> {
    return this.tourSearchService.searchAll(term);
  }
}
