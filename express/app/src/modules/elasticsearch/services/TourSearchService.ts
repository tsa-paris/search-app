import { Service } from "typedi";
import client from "../client";
import {
  TourResult,
  tourIndexName,
  tourSearchFields
} from "../types/TourResult";

@Service()
export default class TourSearchService {
  // constructor(private readonly client: Client) {}
  public async searchAll(term: string = ""): Promise<TourResult[] | null> {
    const result = await client.search(
      {
        index: tourIndexName,
        body: {
          query: {
            multi_match: {
              query: term,
              fields: tourSearchFields
            }
          }
        }
      },
      {
        ignore: [404],
        maxRetries: 3
      }
    );
    const { hits } = result.body;
    return hits.hits;
  }

  public searchOne(): any {
    // TODO
  }

  public create(): any {
    // TODO
  }

  public update(): any {
    // TODO
  }
  public delete(): any {
    // TODO
  }
}
