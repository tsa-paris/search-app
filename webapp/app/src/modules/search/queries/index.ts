import gql from "graphql-tag";

export const SEARCH_RESULTS_QUERY = gql`
  query SearchResultsQuery($term: String) {
    searchAll(term: $term) {
      _id
      _score
      _source {
        id
        code
        name
        agent
        start
        content
        url
      }
    }
  }
`;
