import Vue from "vue";
import VueApollo from "vue-apollo";
import VueRouter from "vue-router";
import App from "./App.vue";
import "./registerServiceWorker";
import store from "./store";
import apolloProvider from "./utils/apollo";
import routes from "./utils/routes";

Vue.config.productionTip = false;

/**
 * VERY IMPORTANT
 * Need to be able to use Apollo Components like Apolloquery
 */
Vue.use(VueApollo);
Vue.use(VueRouter);

const router = new VueRouter({ mode: "history", routes });

new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount("#app");
