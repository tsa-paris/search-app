import VueApollo from "vue-apollo";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { onError } from "apollo-link-error";
import { InMemoryCache } from "apollo-cache-inmemory";

const { VUE_APP_EXPRESS_PORT, NODE_ENV } = process.env;

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.map(
      ({ message, locations, path }) =>
        isDev &&
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
        )
    );
  if (networkError) {
    isDev && console.log(`[Network error]: ${networkError}`);
  }
});

const isDev = NODE_ENV !== "production";
const port = parseInt(VUE_APP_EXPRESS_PORT || "4000", 10) || 4000;
const uri = isDev ? `http://127.0.0.1:${port}/graphql` : "/graphql";

const httpLink = createHttpLink({ uri });

const cache = new InMemoryCache();

const apolloClient = new ApolloClient({
  link: errorLink.concat(httpLink),
  cache,
  connectToDevTools: process.env.NODE_ENV !== "prod"
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});

export default apolloProvider;
