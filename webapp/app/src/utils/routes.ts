import HomeComponent from "../home-component.vue";
import SearchComponent from "../modules/search/search-component.vue";

export default [
  { path: "/", name: "home", component: HomeComponent },
  { path: "/search", name: "search", component: SearchComponent }
];
